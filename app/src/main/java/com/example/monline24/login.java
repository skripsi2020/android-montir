package com.example.monline24;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class login extends AppCompatActivity {
    private TextInputLayout textInputEmail;
    private TextInputLayout textInputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Forgot Password Button Function
        TextView textView1 = findViewById(R.id.btnForgetPass);
        final String text1 = "Lupa Password?";
        SpannableString ss1 = new SpannableString(text1);
        ClickableSpan clickableSpanForgetPass = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(login.this, forgetPass.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.WHITE);
                ds.setUnderlineText(false);
            }
        };
        ss1.setSpan(clickableSpanForgetPass, 0, 14, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView1.setText(ss1);
        textView1.setMovementMethod(LinkMovementMethod.getInstance());



//        textInputEmail = findViewById(R.id.text_input_email);
//        textInputPassword =findViewById(R.id.text_input_password);

//        Call Function Button Masuk
        configurebtnMasuk();
    }
//    private boolean validateEmail(){
//        String emailInput = textInputEmail.getEditText().getText().toString().trim();
//        if(emailInput.isEmpty()){
//            textInputEmail.setError("Field Can't Be Empty\n");
//            return false;
//        } else{
//            textInputEmail.setError(null);
//            return true;
//        }
//    }
//
//    private boolean validatePassword(){
//        String passwordInput = textInputPassword.getEditText().getText().toString().trim();
//        if(passwordInput.isEmpty()){
//            textInputPassword.setError("Field can't be empty\n");
//            return false;
//        } else{
//            textInputPassword.setError(null);
//            return true;
//        }
//    }


//    public void confirmInput(View v){
//        if(!validateEmail() | !validatePassword()){
//            return;
//        }
//        String input = "Email:" + textInputEmail.getEditText().getText().toString();
//        input += "\n";
//        input += "Password:" + textInputPassword.getEditText().getText().toString();
//
//        Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
//
//    }

    //  Login Button Function
    private void configurebtnMasuk() {
        Button btnMasuk = (Button) findViewById(R.id.btnMasuk);
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(login.this, MainActivity.class));
            }
        });
    }
}

